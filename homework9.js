const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const port = 3000;
const rtAPIv1 = express.Router();

const MongoClient = require('mongodb').MongoClient
    , format = require('util').format;
const url = 'mongodb://127.0.0.1:27017/homework9';

var db, users;

// Database
MongoClient.connect(url, function(err, database) {
    if(err) {
        console.log('Невозможно подключиться к БД. Ошибка:', err)
    } else {
        console.log('Соединение установлено для', url)
    }
    db = database
    users = db.collection('users');
    app.listen(port, function () {
        console.log(`Listening on port ${port}`);
    });
});

app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
app.use("/api/v1", rtAPIv1);
app.use(function (err, req, res, next) {
    res.send('ERROR')
});

// REST
rtAPIv1.get("/users/", function (req, res) {
    // let usersLimited = users.slice();
    // let limit = req.query.limit;
    // let offset = req.query.offset;
    //
    // if (limit || offset)
    //     res.send(usersLimited.splice(offset, limit));
    // else
    //     res.send(users);

    users.find().toArray(function (err, results) {
        if (err) {
            console.log(err);
        } else if (results.length) {
            res.send(results);
        } else {
            res.send('Нет документов с данным условием поиска');
        }
    });
});

rtAPIv1.post("/users/", function (req, res) {
    let deleteAll = req.query.deleteAll;

    if (deleteAll === '1')
        users.remove();
    else {
        users.save(req.query, function (err, docs) {
            if (err) {
                console.log(err);
            }
        });
    }

    users.find().toArray(function (err, results) {
        if (err) {
            console.log(err);
        } else if (results.length) {
            res.send(results);
        } else {
            res.send('Нет документов с данным условием поиска');
        }
    });
});

rtAPIv1.get("/users/:id", function (req, res) {
    let id = req.params.id;

    users.find({ "$or": [
        { "name": { "$regex": id} },
        { "surname": { "$regex": id }},
        { "phone": { "$regex": id }}
    ]}).toArray(function (err, results) {
        if (err) {
            console.log(err);
        } else if (results.length) {
            res.send(results);
        } else {
            res.send('Нет документов с данным условием поиска');
        }
    });
});

rtAPIv1.put("/users/:id", function (req, res) {
    let id = req.params.id;
    let name = req.query.name ? req.query.name : null;
    let surname = req.query.surname ? req.query.surname : null;
    let phone = req.query.phone ? req.query.phone : null;

    users.update(
        { "$or": [
            { "name": { "$regex": id} },
            { "surname": { "$regex": id }},
            { "phone": { "$regex": id }}
        ]},
        { $set: {
            name: name,
            surname: surname,
            phone: phone
        }},
        { multi: true }
    );
    users.find().toArray(function (err, results) {
        if (err) {
            console.log(err);
        } else if (results.length) {
            res.send(results);
        } else {
            res.send('Нет документов с данным условием поиска');
        }
    });
});

rtAPIv1.delete("/users/:id", function (req, res) {
    let id = req.params.id;

    users.remove({ "$or": [
        { "name": { "$regex": id} },
        { "surname": { "$regex": id }},
        { "phone": { "$regex": id }}
    ]});
    users.find().toArray(function (err, results) {
        if (err) {
            console.log(err);
        } else if (results.length) {
            res.send(results);
        } else {
            res.send('Нет документов с данным условием поиска');
        }
    });
});

